   
   
   const fetchData = async () => {
// 3 and 4 retrieve all todo list
   await fetch('https://jsonplaceholder.typicode.com/todos')
   .then((response)=>response.json())
   .then((json) => json.map(({ title }) => ({ title })))
   .then((mapResult) => console.log(mapResult));


// 5. Retrieve a single todo list
   await fetch('https://jsonplaceholder.typicode.com/todos/1')
	.then((response)=>response.json())
	.then((json) => console.log(json));

// 6. Print title and status
    await fetch('https://jsonplaceholder.typicode.com/todos/1')
	.then((response)=>response.json())
	.then((json) => console.log(`The item ${json.title} on the list has a status of ${json.completed}`));

// 7. create new todo list
    await fetch('https://jsonplaceholder.typicode.com/todos',{
				method:'POST',
				headers:{
					'Content-Type': 'application/json'
				},
				body: JSON.stringify({
					title: 'Created to be list item',
					completed: false,
					userId: 1
				}),
		})
		.then((response) => response.json())
		.then((json) => console.log(json));


    
// 8. Update to do list item
await fetch("https://jsonplaceholder.typicode.com/todos/1", {
    method: "PUT",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify({
      title: "This is an Updated Title",
      completed: "true",
      userId: 1,
    }),
  })
    .then((res) => res.json())
    .then((converted) => console.log(converted));

// 9. Update a to do list item by changing the data structure
  await fetch("https://jsonplaceholder.typicode.com/todos/5", {
    method: "PUT",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify({
      title: "Updated to do list item",
      description: "To update the my to do list with a different data structure",
      status: "Completed",
      date_completed: "Pending",
      userId: 1,
    }),
  })
    .then((res) => res.json())
    .then((converted) => console.log(converted));

// 10. Create a fetch request using the PATCH
  await fetch("https://jsonplaceholder.typicode.com/todos/5", {
    method: "PATCH",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify({
      description: "Updated Description",
    }),
  })
    .then((res) => res.json())
    .then((converted) => console.log(converted));

// 11. Update a to do list item by changing the status to complete
  await fetch("https://jsonplaceholder.typicode.com/todos/5", {
    method: "PATCH",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify({
      status: "Completed",
      date_completed: "Just Now",
    }),
  })
    .then((res) => res.json())
    .then((converted) => console.log(converted));

// 12. delete an item
  await fetch("https://jsonplaceholder.typicode.com/todos/18", {
    method: "DELETE",
  });





}

    fetchData();

